# WebRTC

WebRTC asynchronous input output real time communication (aiortc) example. Taken from source: https://github.com/jlaine/aiortc.git
Android app mozilla firefox: https://apkpure.com/firefox-browser-fast-private/org.mozilla.firefox

# Paleidimas (Ubuntu 18.04):

#1. cd ~/aiortc/examples/server
#2. sudo python3 server.py
#3. atisiusti ar/ir paleisti mozilla firefox telefone ivedant kompiuterio ("serverio") ip ir 8080 porta. PVZ: 192.168.0.101:8080